 require_relative 'main.rb'

name = 'Tester'

! -> { 'name equals "Tester"' }
+ -> do
  raise Exception.new("name doesn't equals \"Tester\"") if name != "Tester"  
  "name equals \"Tester\""
end

puts
~ -> { '' }
puts

! -> { 'name equals "Rosemary"' }
+ -> do
  raise Exception.new("name doesn't equals \"Rosemary\"") if name != "Rosemary"  
  "name equals \"Rosemary\""
end