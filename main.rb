require "pastel"

module RDow
	module Test
		@@pastel = Pastel.new(enabled: $stderr.tty?)
		extend Module.new { attr_accessor :out, :count }

		def +@
			begin
				warn "#{@@pastel.bright_cyan("\uf41e")} #{@@pastel.dim("#{call}: ok")}"
				RDow::Test.count ||= 0; RDow::Test.count += 1
				return true
			rescue Exception => e
				warn "#{@@pastel.bright_red("\uf467")} #{@@pastel.dim("#{e}: fail")}"
				RDow::Test.count ||= 0; RDow::Test.count -= 1
				return false
			end
		end

		def -@
			begin
				warn "#{@@pastel.bright_red("\uf467")} #{@@pastel.dim("#{call}: fail")}"
				RDow::Test.count ||= 0; RDow::Test.count -= 1
				return false
			rescue Exception => e
				warn "#{@@pastel.bright_cyan("\uf41e")} #{@@pastel.dim("#{e}: ok")}"
				RDow::Test.count ||= 0; RDow::Test.count += 1
				return true
			end
		end

  	def ~
			warn "#{@@pastel.bright_yellow("\uf444")} #{@@pastel.dim("#{call}: pending")}"
  	end
  	
  	def !
  		warn "\uf469 #{@@pastel.dim("info: #{call}")}"
  	end
  end
end

Proc.send :include, RDow::Test
