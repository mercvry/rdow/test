**RDow::Test** is a dead simple test framework

#### features
- small implementation (30 SLOC)
- concise syntax
- color (you need to install pastel gem)
- nerd fonts

#### usage
```ruby
# Block that should succeed
+ -> { 2 + 2 == 4 }
# Block that should fail
- -> { raise }
# Block that is pending
~ -> { }
# Description
! -> { "Description." }
```

#### todo
nothing